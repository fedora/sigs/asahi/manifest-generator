from datetime import date, timedelta
from chalice import Chalice
from pathlib import Path
import json
import boto3

app = Chalice(app_name="manifest-generator")
app.debug = True


class ManifestGenerator:
    """Generate Asahi Installer manifest"""

    def __init__(self):
        self.manifest_path = "__MANIFEST_PATH__"
        self.bucket = "__S3_BUCKET__"
        self.s3 = boto3.client("s3")
        self.num_images_per_build = 3
        self.max_age = timedelta(days=30)
        # since old builds are filtered out we have to drop at most one release
        self.drop_release = "fedora-39"

    def custom_sort_key(self, s):
        parts = s.split("-")
        release = parts[1]  # '38' or 'rawhide'
        flavor = parts[2]  # 'kde', 'gnome', 'server', 'minimal', etc.
        timestamp = parts[-1]  # The timestamp at the end of the string

        # We always want rawhide last
        release_value = int(release) if release.isdigit() else float("inf")

        # We want the flavors in a specific order
        flavor_order = {"kde": 0, "gnome": 1, "server": 2, "minimal": 3}
        flavor_value = flavor_order.get(flavor, float("inf"))

        # Group by release first, then flavor, then timestamp (most recent first)
        return (release_value, flavor_value, -int(timestamp))

    def run(self, body):
        print(f"Trigger: {body}")
        entries = {}

        cutoff_date = date.today() - self.max_age

        paginator = self.s3.get_paginator("list_objects_v2")
        pages = paginator.paginate(Bucket=self.bucket, Prefix="os/")

        for page in pages:
            for obj in page["Contents"]:
                if obj["LastModified"].date() < cutoff_date:
                    continue
                path = Path(obj["Key"])
                if path.suffix == ".json" and not path.stem.startswith(self.drop_release):
                    entries[path.stem] = json.load(
                        self.s3.get_object(Bucket=self.bucket, Key=obj["Key"])["Body"]
                    )

        prev_key = ""
        count = 0
        manifest = {"os_list": []}
        for entry in sorted(entries.keys(), key=self.custom_sort_key):
            key, timestamp = entry.rsplit("-", maxsplit=1)
            if "13.5" not in entries[entry]["supported_fw"]:
                continue
            if "extras" not in entries[entry]:
                continue
            if key != prev_key:
                count = 0
                prev_key = key
            count += 1
            if count > self.num_images_per_build:
                continue
            manifest["os_list"].append(entries[entry])

        self.s3.put_object(
            Bucket=self.bucket, Key=self.manifest_path, Body=json.dumps(manifest)
        )


@app.on_sqs_message(queue="__SQS_QUEUE__", batch_size=1)
def handle_sqs_message(event):
    generator = ManifestGenerator()
    for record in event:
        generator.run(record.body)
