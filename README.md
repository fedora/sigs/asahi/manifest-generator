# manifest-generator

This is a simple Lambda built with AWS Chalice to regenerate the Asahi Installer manifest (`installer-data.json`) whenever a message is received from a SQS queue.
